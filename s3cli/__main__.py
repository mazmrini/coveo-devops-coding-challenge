import sys

from s3cli.infra.boto3.Boto3S3BucketClient import Boto3S3BucketClient
from s3cli.infra.boto3.Boto3S3ObjectsClient import Boto3S3ObjectsClient
from s3cli.infra.S3BucketRepositoryImpl import S3BucketRepositoryImpl

from s3cli.services.filters.S3BucketFilterFactory import S3BucketFilterFactory
from s3cli.services.filters.S3ObjectFilterFactory import S3ObjectFilterFactory
from s3cli.services.S3Service import S3Service

from s3cli.cli.Cli import Cli
from s3cli.cli.utils import generate_cli_params

s3_bucket_repo = S3BucketRepositoryImpl(Boto3S3BucketClient(), Boto3S3ObjectsClient())
s3_service = S3Service(s3_bucket_repo, S3BucketFilterFactory(), S3ObjectFilterFactory())
cli = Cli(s3_service)

params = generate_cli_params(sys.argv[1:])

print(cli.execute(params))
