import unittest

from mockito import mock, unstub, when
from mockito.matchers import ANY
from datetime import datetime

from s3cli.core.S3Bucket import S3Bucket, EmptyS3BucketException
from s3cli.core.S3Object import S3Object
from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter


class S3BucketTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._oldest_s3_object = S3Object(key="oldest_s3_object",
                                          last_modified_date=datetime(1990, 10, 22),
                                          size_in_bytes=700,
                                          storage_type="storage")
        self._most_recent_s3_object = S3Object(key="most_recent_s3_object",
                                               last_modified_date=datetime(2020, 10, 22),
                                               size_in_bytes=300,
                                               storage_type="storage")

        s3_objects = [self._oldest_s3_object, self._most_recent_s3_object, self._oldest_s3_object]
        self._total_size = 1700
        self._nb_files = 3

        self._s3_bucket = S3Bucket(name="s3bucket",
                                   creation_date=datetime.now(),
                                   region="region",
                                   objects=s3_objects)

    def tearDown(self) -> None:
        unstub()

    def test_givenS3Bucket_whenGettingNbFiles_thenReturnsTheExpectedNumberOfFiles(self) -> None:
        result = self._s3_bucket.get_nb_files()

        self.assertEqual(self._nb_files, result)

    def test_givenEmptyS3Bucket_whenGettingNbFiles_thenReturnsZero(self) -> None:
        self._given_empty_bucket()

        result = self._s3_bucket.get_nb_files()

        self.assertEqual(0, result)

    def test_givenS3Bucket_whenGettingTotalSizeInBytes_thenReturnsTheCombinedSizeOfAllS3Objects(self) -> None:
        result = self._s3_bucket.get_total_size_in_bytes()

        self.assertEqual(self._total_size, result)

    def test_givenEmptyS3Bucket_whenGettingTotalSizeInBytes_thenReturnsZero(self) -> None:
        self._given_empty_bucket()

        result = self._s3_bucket.get_total_size_in_bytes()

        self.assertEqual(0, result)

    def test_givenS3Bucket_whenGettingMostRecentObject_thenReturnsTheMostRecentOne(self) -> None:
        result = self._s3_bucket.get_most_recent_object()

        self.assertEqual(self._most_recent_s3_object, result)

    def test_givenEmptyS3Bucket_whenGettingMostRecentObject_thenThrowsEmptyS3BucketException(self) -> None:
        self._given_empty_bucket()

        with self.assertRaises(EmptyS3BucketException):
            self._s3_bucket.get_most_recent_object()

    def test_givenFiltersOnlyKeepsMostRecentObject_whenFilteringObjects_thenReturnsTheSameS3BucketButWithOnlyTheMostRecentObject(self) -> None:
        first_filter, second_filter = mock(S3ObjectFilter), mock(S3ObjectFilter)
        when(first_filter).filter(ANY).thenReturn(True)
        when(second_filter).filter(self._oldest_s3_object).thenReturn(False)
        when(second_filter).filter(self._most_recent_s3_object).thenReturn(True)
        filters = [first_filter, second_filter]
        expected = S3Bucket(
            name=self._s3_bucket.name,
            creation_date=self._s3_bucket.creation_date,
            region=self._s3_bucket.region,
            objects=[self._most_recent_s3_object]
        )

        result = self._s3_bucket.filter_objects(filters)

        self.assertEqual(expected, result)

    def test_givenAlwaysFalseFilter_whenFilteringObjects_thenReturnsTheSameS3BucketButWithNoObjects(self) -> None:
        first_filter, second_filter = mock(S3ObjectFilter), mock(S3ObjectFilter)
        when(first_filter).filter(ANY).thenReturn(False)
        when(second_filter).filter(ANY).thenReturn(False)
        filters = [first_filter, second_filter]
        expected = S3Bucket(
            name=self._s3_bucket.name,
            creation_date=self._s3_bucket.creation_date,
            region=self._s3_bucket.region,
            objects=[]
        )

        result = self._s3_bucket.filter_objects(filters)

        self.assertEqual(expected, result)

    def test_givenAlwaysTrueFilter_whenFilteringObjects_thenReturnsTheSameS3Bucket(self) -> None:
        first_filter, second_filter = mock(S3ObjectFilter), mock(S3ObjectFilter)
        when(first_filter).filter(ANY).thenReturn(True)
        when(second_filter).filter(ANY).thenReturn(True)
        filters = [first_filter, second_filter]

        result = self._s3_bucket.filter_objects(filters)

        self.assertEqual(self._s3_bucket, result)

    def test_givenEmptyFilters_whenFilteringObjects_thenReturnsTheSameS3Bucket(self) -> None:
        result = self._s3_bucket.filter_objects([])

        self.assertEqual(self._s3_bucket, result)

    def _given_empty_bucket(self) -> None:
        self._s3_bucket.objects = []
