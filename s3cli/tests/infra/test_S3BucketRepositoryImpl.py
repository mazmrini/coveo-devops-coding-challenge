import unittest
from mockito import mock, unstub, when
from datetime import datetime

from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.S3Object import S3Object
from s3cli.infra.S3BucketRepositoryImpl import S3BucketRepositoryImpl
from s3cli.infra.interfaces import S3BucketClient, S3ObjectsClient, S3BucketDto


class S3BucketRepositoryImplTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._s3_bucket_client = mock(S3BucketClient)
        self._first_bucket = S3BucketDto(name="first_bucket", creation_date=datetime.now(), location="location_1")
        self._second_bucket = S3BucketDto(name="second_bucket", creation_date=datetime.now(), location="location_2")
        when(self._s3_bucket_client).get_all_buckets().thenReturn([self._first_bucket, self._second_bucket])

        self._s3_objects_client = mock(S3ObjectsClient)
        self._first_bucket_objects = [
            S3Object(key="1.png", last_modified_date=datetime.now(), storage_type="storage_1", size_in_bytes=33),
            S3Object(key="2.png", last_modified_date=datetime.now(), storage_type="storage_1", size_in_bytes=88),
            S3Object(key="imgs/33.png", last_modified_date=datetime.now(), storage_type="storage_42", size_in_bytes=18)
        ]
        self._second_bucket_objects = []
        when(self._s3_objects_client).get_all_objects(self._first_bucket.name).thenReturn(self._first_bucket_objects)
        when(self._s3_objects_client).get_all_objects(self._second_bucket.name).thenReturn(self._second_bucket_objects)

        self._repository = S3BucketRepositoryImpl(self._s3_bucket_client, self._s3_objects_client)

    def tearDown(self) -> None:
        unstub()

    def test_givenBucketsWithObjects_whenGettingAllBuckets_thenReturnsExpectedS3BucketList(self) -> None:
        expected_buckets = [
            S3Bucket(
                name=self._first_bucket.name,
                creation_date=self._first_bucket.creation_date,
                region=self._first_bucket.location,
                objects=self._first_bucket_objects
            ),
            S3Bucket(
                name=self._second_bucket.name,
                creation_date=self._second_bucket.creation_date,
                region=self._second_bucket.location,
                objects=self._second_bucket_objects
            )
        ]

        result = self._repository.find_all_buckets()

        self.assertEqual(expected_buckets, result)

    def test_givenNoBuckets_whenGettingAllBuckets_thenReturnsEmptyList(self) -> None:
        when(self._s3_bucket_client).get_all_buckets().thenReturn([])

        result = self._repository.find_all_buckets()

        self.assertEqual([], result)
