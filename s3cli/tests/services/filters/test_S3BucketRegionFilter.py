import unittest
from datetime import datetime

from s3cli.core.S3Bucket import S3Bucket
from s3cli.services.filters.S3BucketRegionFilter import S3BucketRegionFilter


class S3BucketREgionFilterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._s3_basic_metadata = S3Bucket(name="whatev",
                                           creation_date=datetime.now(),
                                           region="region",
                                           objects=[])

    def test_givenMatchingRegion_whenFiltering_thenReturnsTrue(self) -> None:
        region = "some_region"
        basic_filter = S3BucketRegionFilter(region)
        self._s3_basic_metadata.region = region

        result = basic_filter.filter(self._s3_basic_metadata)

        self.assertTrue(result)

    def test_givenDifferentRegion_whenFiltering_thenReturnsFalse(self) -> None:
        region = "some_region"
        self._s3_basic_metadata.region = region
        different_region = "some_different_region"
        basic_filter = S3BucketRegionFilter(different_region)

        result = basic_filter.filter(self._s3_basic_metadata)

        self.assertFalse(result)
