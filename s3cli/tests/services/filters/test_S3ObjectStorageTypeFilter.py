import unittest
from datetime import datetime

from s3cli.core.S3Object import S3Object
from s3cli.services.filters.S3ObjectStorageTypeFilter import S3ObjectStorageTypeFilter


class S3BucketObjectStorageTypeFilterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._object_metadata = S3Object(key="key",
                                         last_modified_date=datetime.now(),
                                         storage_type="storage",
                                         size_in_bytes=0)

    def test_givenMatchingStorageTypes_whenFiltering_thenReturnsTrue(self) -> None:
        storage_type = "storage_type"
        self._object_metadata.storage_type = storage_type
        object_filter = S3ObjectStorageTypeFilter(storage_type)

        result = object_filter.filter(self._object_metadata)

        self.assertTrue(result)

    def test_givenDifferentStorageTypes_whenFiltering_thenReturnsFalse(self) -> None:
        storage_type = "storage_type"
        self._object_metadata.storage_type = storage_type
        different_storage_type = "different_storage_type"
        object_filter = S3ObjectStorageTypeFilter(different_storage_type)

        result = object_filter.filter(self._object_metadata)

        self.assertFalse(result)
