import unittest
from mockito import when, unstub
from datetime import datetime
import re

from s3cli.core.S3Object import S3Object
from s3cli.services.filters.S3ObjectFileRegexpFilter import S3ObjectFileRegexpFilter
from s3cli.services.filters.S3ObjectStorageTypeFilter import S3ObjectStorageTypeFilter


class S3BucketObjectFileRegexpFilterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._object_metadata = S3Object(key="key",
                                         last_modified_date=datetime.now(),
                                         storage_type="storage",
                                         size_in_bytes=0)

    def tearDown(self) -> None:
        unstub()

    def test_givenRegexpMatches_whenFiltering_thenReturnsTrue(self) -> None:
        regexp = "regexp"
        object_filter = S3ObjectFileRegexpFilter(regexp)
        self._object_metadata.key = "key"

        with when(re).search(regexp, self._object_metadata.key).thenReturn(True):
            result = object_filter.filter(self._object_metadata)

        self.assertTrue(result)

    def test_givenRegexpDoesNotMatch_whenFiltering_thenReturnsFalse(self) -> None:
        regexp = "regexp"
        object_filter = S3ObjectFileRegexpFilter(regexp)
        self._object_metadata.key = regexp

        with when(re).search(regexp, self._object_metadata.key).thenReturn(False):
            result = object_filter.filter(self._object_metadata)

        self.assertFalse(result)
