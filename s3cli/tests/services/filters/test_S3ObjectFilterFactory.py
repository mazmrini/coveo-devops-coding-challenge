import unittest

from s3cli.services.filters.S3ObjectFileRegexpFilter import S3ObjectFileRegexpFilter
from s3cli.services.filters.S3ObjectFilterFactory \
    import S3ObjectFilterFactory, UnknownObjectFilterException
from s3cli.services.filters.S3ObjectFileOnlyFilter import S3ObjectFileOnlyFilter
from s3cli.services.filters.S3ObjectStorageTypeFilter import S3ObjectStorageTypeFilter
from s3cli.services.filters.query import FilterQuery


class S3ObjectFilterFactoryTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.__factory = S3ObjectFilterFactory()

    def test_givenFileOnlyFilter_whenMakingIt_thenReturnsFileOnlyFilter(self) -> None:
        query = self._make_filter_query("file_only")

        result = self.__factory.make_filter(query)

        self.assertIsInstance(result, S3ObjectFileOnlyFilter)

    def test_givenStorageTypeFilter_whenMakingIt_thenReturnsStorageTypeFilter(self) -> None:
        query = self._make_filter_query("storage_type", "expected_type")

        result = self.__factory.make_filter(query)

        self.assertIsInstance(result, S3ObjectStorageTypeFilter)

    def test_givenRegexpFilter_whenMakingIt_thenReturnsFileRegexpFilter(self) -> None:
        query = self._make_filter_query("regexp", "some_Regexp")

        result = self.__factory.make_filter(query)

        self.assertIsInstance(result, S3ObjectFileRegexpFilter)

    def test_givenUnknownFilter_whenMakingIt_thenThrowsUnkownFilterException(self) -> None:
        query = self._make_filter_query(name="unknown")

        with self.assertRaises(UnknownObjectFilterException):
            self.__factory.make_filter(query)

    def _make_filter_query(self, name: str, params: object = None):
        return FilterQuery(filter_name=name, filter_params=params)

