import unittest
from datetime import datetime

from s3cli.core.S3Bucket import S3Bucket
from s3cli.services.filters.S3BucketNameFilter import S3BucketNameFilter


class S3BucketNameFilterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._s3_basic_metadata = S3Bucket(name="whatev",
                                           creation_date=datetime.now(),
                                           region="region",
                                           objects=[])

    def test_givenMatchingNames_whenFiltering_thenReturnsTrue(self) -> None:
        name = "some_name"
        basic_filter = S3BucketNameFilter(name)
        self._s3_basic_metadata.name = name

        result = basic_filter.filter(self._s3_basic_metadata)

        self.assertTrue(result)

    def test_givenDifferentNames_whenFiltering_thenReturnsFalse(self) -> None:
        name = "some_name"
        self._s3_basic_metadata.name = name
        different_name = "some_different_name"
        basic_filter = S3BucketNameFilter(different_name)

        result = basic_filter.filter(self._s3_basic_metadata)

        self.assertFalse(result)
