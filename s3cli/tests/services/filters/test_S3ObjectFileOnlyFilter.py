import unittest
from datetime import datetime

from s3cli.core.S3Object import S3Object
from s3cli.services.filters.S3ObjectFileOnlyFilter import S3ObjectFileOnlyFilter


class S3ObjectStorageTypeFilterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._object_metadata = S3Object(key="key",
                                         last_modified_date=datetime.now(),
                                         storage_type="storage",
                                         size_in_bytes=0)

        self._file_only_filter = S3ObjectFileOnlyFilter()

    def test_givenObjectIsAFileAtRoot_whenFiltering_thenReturnsTrue(self) -> None:
        self._object_metadata.key = "img.png"

        result = self._file_only_filter.filter(self._object_metadata)

        self.assertTrue(result)

    def test_givenObjectIsAFileInASubDirectory_whenFiltering_thenReturnsTrue(self) -> None:
        self._object_metadata.key = "solo/resources/static/img.png"

        result = self._file_only_filter.filter(self._object_metadata)

        self.assertTrue(result)

    def test_givenObjectIsADirectory_whenFiltering_thenReturnsFalse(self) -> None:
        self._object_metadata.key = "directories_ends_with_a_forward_slash/"

        result = self._file_only_filter.filter(self._object_metadata)

        self.assertFalse(result)

    def test_givenObjectIsASubDirectory_whenFiltering_thenReturnsFalse(self) -> None:
        self._object_metadata.key = "solo/resources/static/"

        result = self._file_only_filter.filter(self._object_metadata)

        self.assertFalse(result)
