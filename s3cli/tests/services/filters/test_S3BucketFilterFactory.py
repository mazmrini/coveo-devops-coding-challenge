import unittest

from s3cli.services.filters.S3BucketFilterFactory \
    import S3BucketFilterFactory, UnknownBucketFilterException
from s3cli.services.filters.S3BucketNameFilter import S3BucketNameFilter
from s3cli.services.filters.S3BucketRegionFilter import S3BucketRegionFilter
from s3cli.services.filters.query import FilterQuery


class S3BucketFilterFactoryTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.__factory = S3BucketFilterFactory()

    def test_givenNameFilter_whenMakingIt_thenReturnsNameFilter(self) -> None:
        query = self._make_filter_query("name", "expected_name")

        result = self.__factory.make_filter(query)

        self.assertIsInstance(result, S3BucketNameFilter)

    def test_givenRegionFilter_whenMakingIt_thenReturnsRegionFilter(self) -> None:
        query = self._make_filter_query("region", "expected_region")

        result = self.__factory.make_filter(query)

        self.assertIsInstance(result, S3BucketRegionFilter)

    def test_givenUnknownFilter_whenMakingIt_thenThrowsUnkownFilterException(self) -> None:
        query = self._make_filter_query("unknown", "expected_name")

        with self.assertRaises(UnknownBucketFilterException):
            self.__factory.make_filter(query)

    def _make_filter_query(self, name: str, params: object):
        return FilterQuery(filter_name=name, filter_params=params)
