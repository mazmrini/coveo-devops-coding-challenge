import unittest
from datetime import datetime

from mockito import when, mock, unstub

from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.S3BucketRepository import S3BucketRepository
from s3cli.services.S3Service import S3Service
from s3cli.services.filters.S3BucketFilterFactory import S3BucketFilterFactory
from s3cli.services.filters.S3ObjectFilterFactory import S3ObjectFilterFactory


class S3ServiceTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._s3_bucket_repository = mock(S3BucketRepository)
        self._s3_bucket_filter_factory = mock(S3BucketFilterFactory)
        self._s3_object_filter_factory = mock(S3ObjectFilterFactory)

        self._first_s3_bucket_objects = [

        ]
        self._first_s3_bucket = S3Bucket(
            name="first_bucket",
            creation_date=datetime.now(),
            region="first_region",
            objects=[]
        )

        self._s3_service = S3Service(self._s3_bucket_repository,
                                     self._s3_bucket_filter_factory,
                                     self._s3_object_filter_factory)

    def tearDown(self) -> None:
        unstub()
