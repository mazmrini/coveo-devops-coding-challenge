import unittest

from mockito import mock, unstub, when, verify

from s3cli.cli.Cli import Cli, CliParams
from s3cli.cli.ResultFormatter import ResultFormatter
from s3cli.services.S3Service import S3Service
from s3cli.services.dtos import S3BucketMetadata
from s3cli.services.filters.query import FilterQuery


class CliTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._bucket_filters = [mock(FilterQuery), mock(FilterQuery), mock(FilterQuery)]
        self._object_filters = [mock(FilterQuery), mock(FilterQuery)]
        self._formatter = mock(ResultFormatter)
        self._s3_service = mock(S3Service)

        buckets_metadata = [mock(S3BucketMetadata), mock(S3BucketMetadata)]
        when(self._s3_service)\
            .get_buckets_metadata(self._bucket_filters, self._object_filters)\
            .thenReturn(buckets_metadata)
        self._formatted_result = "WELL_DONE"
        when(self._formatter).format(buckets_metadata).thenReturn(self._formatted_result)

        self._cli = Cli(self._s3_service)

    def tearDown(self) -> None:
        unstub()

    def test_givenCliParams_whenExecuting_thenReturnsTheFormatterResult(self) -> None:
        result = self._cli.execute(CliParams(
            bucket_filters=self._bucket_filters,
            object_filters=self._object_filters,
            formatter=self._formatter
        ))

        self.assertEqual(self._formatted_result, result)

    def test_givenCliParams_whenExecuting_thenTheServiceIsCalledWithTheFilters(self) -> None:
        self._cli.execute(CliParams(
            bucket_filters=self._bucket_filters,
            object_filters=self._object_filters,
            formatter=self._formatter
        ))

        verify(self._s3_service).get_buckets_metadata(self._bucket_filters, self._object_filters)
