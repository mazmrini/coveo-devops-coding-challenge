import unittest
from datetime import datetime
from typing import List

from s3cli.cli.SizeDisplayMapper import SizeDisplayMapper
from s3cli.services.dtos import S3BucketMetadata


class SizeDisplayMapperTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._first_creation_time = datetime.now()
        self._second_creation_time = datetime.now()
        self._third_creation_time = datetime.now()

    def test_givenS3BucketsWith10bytes20bytesAnd30bytesInTotalSize_whenMappingToLowerB_thenReturnsS3BucketsWithTheSameBytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("b")

        result = mapper.map(buckets)

        self.assertEqual(buckets, result)

    def test_givenS3BucketsWith10bytes20bytesAnd30bytesInTotalSize_whenMappingToUpperB_thenReturnsS3BucketsWithTheSameBytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("b")

        result = mapper.map(buckets)

        self.assertEqual(buckets, result)

    def test_givenS3BucketsWith10Kb20KbAnd30KbInTotalSize_whenMappingToUpperKB_thenReturnsS3BucketsWithTheKilobytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000, 20_000, 30_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("KB")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Kb20KbAnd30KbInTotalSize_whenMappingToLowerKB_thenReturnsS3BucketsWithTheKilobytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000, 20_000, 30_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("KB")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Kb20KbAnd30KbInTotalSize_whenMappingToUpperMB_thenReturnsS3BucketsWithTheMegabytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000_000, 20_000_000, 30_000_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("MB")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Mb20MbAnd30MbInTotalSize_whenMappingToLowerMB_thenReturnsS3BucketsWithTheMegabytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000_000, 20_000_000, 30_000_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("mb")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Gb20GbAnd30GbInTotalSize_whenMappingToUpperGB_thenReturnsS3BucketsWithTheGigabytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000_000_000, 20_000_000_000, 30_000_000_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("GB")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Gb20GbAnd30GbInTotalSize_whenMappingToLowerGB_thenReturnsS3BucketsWithTheGigabytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000_000_000, 20_000_000_000, 30_000_000_000)
        expected = self.__make_S3BucketsMetadata(10, 20, 30)
        mapper = SizeDisplayMapper("gb")

        result = mapper.map(buckets)

        self.assertEqual(expected, result)

    def test_givenS3BucketsWith10Gb20GbAnd30GbInTotalSize_whenMappingToUnkown_thenReturnsS3BucketsWithTheBytesValues(self) -> None:
        buckets = self.__make_S3BucketsMetadata(10_000_000_000, 20_000_000_000, 30_000_000_000)
        mapper = SizeDisplayMapper("unknown")

        result = mapper.map(buckets)

        self.assertEqual(buckets, result)

    def __make_S3BucketsMetadata(self, first_size: int, second_size: int, third_size: int) -> List[S3BucketMetadata]:
        return [
            S3BucketMetadata(
                name="first",
                creation_date=self._first_creation_time,
                region="first_region",
                total_files_size_in_bytes=float(first_size),
                nb_of_files=10,
                last_modified_date_of_most_recent_file=self._first_creation_time
            ),
            S3BucketMetadata(
                name="second",
                creation_date=self._second_creation_time,
                region="second_region",
                total_files_size_in_bytes=float(second_size),
                nb_of_files=20,
                last_modified_date_of_most_recent_file=self._second_creation_time
            ),
            S3BucketMetadata(
                name="third",
                creation_date=self._third_creation_time,
                region="third_region",
                total_files_size_in_bytes=float(third_size),
                nb_of_files=30,
                last_modified_date_of_most_recent_file=self._third_creation_time
            )
        ]
