from abc import ABCMeta, abstractmethod
from typing import List

from s3cli.core.S3Bucket import S3Bucket


class S3BucketRepository(metaclass=ABCMeta):
    @abstractmethod
    def find_all_buckets(self) -> List[S3Bucket]:
        pass
