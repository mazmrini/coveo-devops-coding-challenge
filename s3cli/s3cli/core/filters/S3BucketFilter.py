from abc import ABCMeta, abstractmethod

from s3cli.core.S3Bucket import S3Bucket


class S3BucketFilter(metaclass=ABCMeta):
    @abstractmethod
    def filter(self, bucket: S3Bucket) -> bool:
        pass
