from abc import ABCMeta, abstractmethod

from s3cli.core.S3Object import S3Object


class S3ObjectFilter(metaclass=ABCMeta):
    @abstractmethod
    def filter(self, s3_object: S3Object) -> bool:
        pass
