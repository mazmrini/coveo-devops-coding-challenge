from typing import List, Iterable
from datetime import datetime
from dataclasses import dataclass

from s3cli.core.S3Object import S3Object
from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter


@dataclass
class S3Bucket:
    name: str
    creation_date: datetime
    region: str
    objects: List[S3Object]

    def filter_objects(self, filters: List[S3ObjectFilter]) -> 'S3Bucket':
        filtered_objects: Iterable = self.objects
        for obj_filter in filters:
            filtered_objects = filter(obj_filter.filter, filtered_objects)

        return S3Bucket(
            name=self.name,
            region=self.region,
            creation_date=self.creation_date,
            objects=list(filtered_objects)
        )

    def get_nb_files(self) -> int:
        return len(self.objects)

    def get_total_size_in_bytes(self) -> int:
        total = 0
        for s3_object in self.objects:
            total += s3_object.size_in_bytes

        return total

    def get_most_recent_object(self) -> S3Object:
        if len(self.objects) == 0:
            raise EmptyS3BucketException()

        most_recent = self.objects[0]
        for s3_object in self.objects:
            if s3_object.last_modified_date > most_recent.last_modified_date:
                most_recent = s3_object

        return most_recent


class EmptyS3BucketException(Exception):
    pass
