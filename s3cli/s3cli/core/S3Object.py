from datetime import datetime
from dataclasses import dataclass


@dataclass
class S3Object:
    key: str
    last_modified_date: datetime
    storage_type: str
    size_in_bytes: int
