from typing import List

from s3cli.cli.mappers import DisplayMapper
from s3cli.services.dtos import S3BucketMetadata


class ResultFormatter:
    def __init__(self, display_mappers: List[DisplayMapper]):
        self.__display_mappers = display_mappers

    def format(self, results: List[S3BucketMetadata]) -> str:
        results = self.__display_mapping(results)

        to_print = ""
        for result in results:
            last_modified = "N/A" if result.last_modified_date_of_most_recent_file is None \
                else result.last_modified_date_of_most_recent_file.isoformat()

            to_print += "\n"
            to_print += f"Name: {result.name}\n"
            to_print += f"Creation: {result.creation_date.isoformat()}\n"
            to_print += f"Region: {result.region}\n"
            to_print += f"Nb files: {result.nb_of_files}\n"
            to_print += f"Total size: {result.total_files_size_in_bytes}\n"
            to_print += f"Last modified file @: {last_modified}\n"

        return to_print

    def __display_mapping(self, results: List[S3BucketMetadata]) -> List[S3BucketMetadata]:
        for mapper in self.__display_mappers:
            results = mapper.map(results)

        return results
