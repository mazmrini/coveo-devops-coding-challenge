from typing import List

from dataclasses import dataclass

from s3cli.cli.ResultFormatter import ResultFormatter
from s3cli.services.S3Service import S3Service
from s3cli.services.filters.query import FilterQuery


@dataclass
class CliParams:
    bucket_filters: List[FilterQuery]
    object_filters: List[FilterQuery]
    formatter: ResultFormatter


class Cli:
    def __init__(self, s3_service: S3Service) -> None:
        self.__s3_service = s3_service

    def execute(self, params: CliParams) -> str:
        result = self.__s3_service.get_buckets_metadata(params.bucket_filters, params.object_filters)

        return params.formatter.format(result)
