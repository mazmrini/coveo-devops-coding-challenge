from typing import List

from s3cli.cli.mappers import DisplayMapper
from s3cli.services.dtos import S3BucketMetadata


class SizeDisplayMapper(DisplayMapper):
    DEFAULT_DIVISOR = "b"

    DIVISORS_MAP = {
        "b": 1,
        "kb": 1_000,
        "mb": 1_000_000,
        "gb": 1_000_000_000
    }

    def __init__(self, divisor_key: str) -> None:
        divisor_key = divisor_key.lower() \
            if divisor_key.lower() in SizeDisplayMapper.DIVISORS_MAP \
            else SizeDisplayMapper.DEFAULT_DIVISOR

        self._divisor = SizeDisplayMapper.DIVISORS_MAP[divisor_key]

    def map(self, s3_buckets: List[S3BucketMetadata]) -> List[S3BucketMetadata]:
        return list(map(self.__map_single_bucket, s3_buckets))

    def __map_single_bucket(self, bucket: S3BucketMetadata) -> S3BucketMetadata:
        return S3BucketMetadata(
            name=bucket.name,
            creation_date=bucket.creation_date,
            region=bucket.region,
            nb_of_files=bucket.nb_of_files,
            total_files_size_in_bytes=bucket.total_files_size_in_bytes / self._divisor,
            last_modified_date_of_most_recent_file=bucket.last_modified_date_of_most_recent_file
        )
