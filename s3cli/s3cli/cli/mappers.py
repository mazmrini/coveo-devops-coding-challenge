from abc import ABCMeta, abstractmethod
from typing import List

from s3cli.services.dtos import S3BucketMetadata


class DisplayMapper(metaclass=ABCMeta):
    @abstractmethod
    def map(self, s3_buckets: List[S3BucketMetadata]) -> List[S3BucketMetadata]:
        pass
