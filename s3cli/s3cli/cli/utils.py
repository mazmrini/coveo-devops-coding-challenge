from typing import List, Dict, Callable

from s3cli.cli.Cli import CliParams
from s3cli.cli.ResultFormatter import ResultFormatter
from s3cli.cli.SizeDisplayMapper import SizeDisplayMapper
from s3cli.cli.mappers import DisplayMapper
from s3cli.services.filters.query import FilterQuery


def generate_cli_params(params: List[str]) -> CliParams:
    return CliParams(
        bucket_filters=__to_bucket_filter_queries(params),
        object_filters=__to_object_filter_queries(params),
        formatter=ResultFormatter(__to_display_mappers(params))
    )


bucket_filters: Dict[str, Callable[[object], FilterQuery]] = {
    "bucket_name": lambda params: FilterQuery(filter_name="name", filter_params=params),
    "bucket_region": lambda params: FilterQuery(filter_name="region", filter_params=params)
}


def __to_bucket_filter_queries(params: List[str]) -> List[FilterQuery]:
    queries = []
    for param in params:
        key, value = param.split("=")
        if key in bucket_filters:
            queries.append(bucket_filters[key](value))

    return queries


object_filters: Dict[str, Callable[[object], FilterQuery]] = {
    "file_storage": lambda params: FilterQuery(filter_name="storage_type", filter_params=params),
    "file_regexp": lambda params: FilterQuery(filter_name="regexp", filter_params=params)
}


def __to_object_filter_queries(params: List[str]) -> List[FilterQuery]:
    queries = [FilterQuery(filter_name="file_only")]
    for param in params:
        key, value = param.split("=")
        if key in object_filters:
            queries.append(object_filters[key](value))

    return queries


display_mappers: Dict[str, Callable[[object], DisplayMapper]] = {
    "sizeIn": lambda size: SizeDisplayMapper(str(size))
}


def __to_display_mappers(params: List[str]) -> List[DisplayMapper]:
    mappers = []
    for param in params:
        key, value = param.split("=")
        if key in display_mappers:
            mappers.append(display_mappers[key](value))

    return mappers
