from typing import List
from abc import ABCMeta, abstractmethod
from datetime import datetime
from dataclasses import dataclass

from s3cli.core.S3Object import S3Object


@dataclass
class S3BucketDto:
    name: str
    creation_date: datetime
    location: str


class S3BucketClient(metaclass=ABCMeta):
    @abstractmethod
    def get_all_buckets(self) -> List[S3BucketDto]:
        pass


class S3ObjectsClient(metaclass=ABCMeta):
    @abstractmethod
    def get_all_objects(self, bucket_name: str) -> List[S3Object]:
        pass
