from datetime import datetime
from typing import List
from dataclasses import dataclass

import boto3

from s3cli.core.S3Object import S3Object
from s3cli.infra.interfaces import S3ObjectsClient


class Boto3S3ObjectsClient(S3ObjectsClient):
    def __init__(self) -> None:
        self.__boto3_resource = boto3.resource('s3')

    def get_all_objects(self, bucket_name: str) -> List[S3Object]:
        bucket = self.__boto3_resource.Bucket(name=bucket_name)

        objects = []
        for obj in bucket.objects.all():
            objects.append(S3Object(
                key=obj.key,
                size_in_bytes=obj.size,
                storage_type=obj.storage_class,
                last_modified_date=obj.last_modified
            ))

        return objects
