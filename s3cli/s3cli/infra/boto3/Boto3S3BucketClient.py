from datetime import datetime
from typing import List
from dataclasses import dataclass

import boto3
from dataclasses_json import DataClassJsonMixin

from s3cli.infra.interfaces import S3BucketClient, S3BucketDto


@dataclass
class Boto3Bucket(DataClassJsonMixin):
    Name: str
    CreationDate: datetime


@dataclass
class Boto3Buckets(DataClassJsonMixin):
    Buckets: List[Boto3Bucket]


@dataclass
class Boto3BucketLocation(DataClassJsonMixin):
    LocationConstraint: str


class Boto3S3BucketClient(S3BucketClient):
    def __init__(self) -> None:
        self.__boto3_client = boto3.client('s3')

    def get_all_buckets(self) -> List[S3BucketDto]:
        buckets = Boto3Buckets.from_dict(self.__boto3_client.list_buckets()).Buckets

        buckets_dtos = []
        for bucket in buckets:
            location = self.__boto3_client.get_bucket_location(Bucket=bucket.Name)

            buckets_dtos.append(S3BucketDto(
                name=bucket.Name,
                creation_date=bucket.CreationDate,
                location=Boto3BucketLocation.from_dict(location).LocationConstraint
            ))

        return buckets_dtos
