from typing import List

from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.S3BucketRepository import S3BucketRepository
from s3cli.infra.interfaces import S3BucketClient, S3ObjectsClient


class S3BucketRepositoryImpl(S3BucketRepository):
    def __init__(self, s3_bucket_client: S3BucketClient, s3_objects_client: S3ObjectsClient) -> None:
        self.__s3_bucket_client = s3_bucket_client
        self.__s3_objects_client = s3_objects_client

    def find_all_buckets(self) -> List[S3Bucket]:
        buckets = self.__s3_bucket_client.get_all_buckets()

        return [
            S3Bucket(
                name=bucket.name,
                creation_date=bucket.creation_date,
                region=bucket.location,
                objects=self.__s3_objects_client.get_all_objects(bucket.name)
            ) for bucket in buckets
        ]
