from datetime import datetime
from typing import Union

from dataclasses import dataclass


@dataclass
class S3BucketMetadata:
    name: str
    region: str
    creation_date: datetime
    nb_of_files: int
    total_files_size_in_bytes: Union[int, float]
    last_modified_date_of_most_recent_file: Union[datetime, None]
