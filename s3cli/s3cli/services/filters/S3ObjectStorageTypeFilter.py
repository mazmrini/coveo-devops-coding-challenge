from s3cli.core.S3Object import S3Object
from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter


class S3ObjectStorageTypeFilter(S3ObjectFilter):
    def __init__(self, storage_type: str) -> None:
        self.__storage_type = storage_type

    def filter(self, s3_object: S3Object) -> bool:
        return s3_object.storage_type == self.__storage_type
