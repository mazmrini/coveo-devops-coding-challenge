from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.filters.S3BucketFilter import S3BucketFilter


class S3BucketRegionFilter(S3BucketFilter):
    def __init__(self, region: str) -> None:
        self.__region = region

    def filter(self, bucket: S3Bucket) -> bool:
        return bucket.region == self.__region
