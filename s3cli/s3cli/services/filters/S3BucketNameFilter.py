from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.filters.S3BucketFilter import S3BucketFilter


class S3BucketNameFilter(S3BucketFilter):
    def __init__(self, name: str) -> None:
        self.__name = name

    def filter(self, bucket: S3Bucket) -> bool:
        return bucket.name == self.__name
