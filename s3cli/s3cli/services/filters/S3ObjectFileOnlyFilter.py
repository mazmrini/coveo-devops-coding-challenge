from s3cli.core.S3Object import S3Object
from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter


class S3ObjectFileOnlyFilter(S3ObjectFilter):
    def filter(self, s3_object: S3Object) -> bool:
        return not self.__is_a_directory(s3_object.key)

    def __is_a_directory(self, path: str) -> bool:
        return path.endswith("/")
