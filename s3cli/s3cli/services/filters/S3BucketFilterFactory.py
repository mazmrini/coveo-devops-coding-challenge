from typing import Dict, Callable

from s3cli.core.filters.S3BucketFilter import S3BucketFilter
from s3cli.services.filters.S3BucketNameFilter import S3BucketNameFilter
from s3cli.services.filters.S3BucketRegionFilter import S3BucketRegionFilter
from s3cli.services.filters.query import FilterQuery


class S3BucketFilterFactory:
    FILTERS_INSTANTIATOR: Dict[str, Callable[[object], S3BucketFilter]] = {
        "name": lambda expected: S3BucketNameFilter(str(expected)),
        "region": lambda expected: S3BucketRegionFilter(str(expected))
    }

    def make_filter(self, query: FilterQuery) -> S3BucketFilter:
        if query.filter_name not in S3BucketFilterFactory.FILTERS_INSTANTIATOR:
            raise UnknownBucketFilterException()

        return S3BucketFilterFactory.FILTERS_INSTANTIATOR[query.filter_name](query.filter_params)


class UnknownBucketFilterException(Exception):
    pass
