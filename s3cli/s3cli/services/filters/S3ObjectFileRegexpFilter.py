import re

from s3cli.core.S3Object import S3Object
from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter


class S3ObjectFileRegexpFilter(S3ObjectFilter):
    def __init__(self, regexp: str) -> None:
        self.__regexp = regexp

    def filter(self, s3_object: S3Object) -> bool:
        return bool(re.search(self.__regexp, s3_object.key))
