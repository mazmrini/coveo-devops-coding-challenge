from typing import Dict, Callable

from s3cli.core.filters.S3ObjectFilter import S3ObjectFilter
from s3cli.services.filters.S3ObjectFileOnlyFilter import S3ObjectFileOnlyFilter
from s3cli.services.filters.S3ObjectFileRegexpFilter import S3ObjectFileRegexpFilter
from s3cli.services.filters.S3ObjectStorageTypeFilter import S3ObjectStorageTypeFilter
from s3cli.services.filters.query import FilterQuery


class S3ObjectFilterFactory:
    FILTERS_INSTANTIATOR: Dict[str, Callable[[object], S3ObjectFilter]] = {
        "file_only": lambda _: S3ObjectFileOnlyFilter(),
        "storage_type": lambda expected: S3ObjectStorageTypeFilter(str(expected)),
        "regexp": lambda regexp: S3ObjectFileRegexpFilter(str(regexp))
    }

    def make_filter(self, query: FilterQuery) -> S3ObjectFilter:
        if query.filter_name not in S3ObjectFilterFactory.FILTERS_INSTANTIATOR:
            raise UnknownObjectFilterException()

        return S3ObjectFilterFactory.FILTERS_INSTANTIATOR[query.filter_name](query.filter_params)


class UnknownObjectFilterException(Exception):
    pass
