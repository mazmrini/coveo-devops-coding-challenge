from dataclasses import dataclass


@dataclass
class FilterQuery:
    filter_name: str = ""
    filter_params: object = None
