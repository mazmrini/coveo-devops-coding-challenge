from datetime import datetime
from typing import List, Iterable, Union

from s3cli.core.S3Bucket import S3Bucket
from s3cli.core.S3BucketRepository import S3BucketRepository
from s3cli.services.dtos import S3BucketMetadata
from s3cli.services.filters.S3BucketFilterFactory import S3BucketFilterFactory
from s3cli.services.filters.S3ObjectFilterFactory import S3ObjectFilterFactory
from s3cli.services.filters.query import FilterQuery


class S3Service:
    def __init__(self, s3_bucket_repository: S3BucketRepository,
                 s3_bucket_filter_factory: S3BucketFilterFactory,
                 s3_object_filter_factory: S3ObjectFilterFactory) -> None:
        self.__s3_bucket_repository = s3_bucket_repository
        self.__s3_bucket_filter_factory = s3_bucket_filter_factory
        self.__s3_object_filter_factory = s3_object_filter_factory

    def get_buckets_metadata(self, bucket_filter_queries: List[FilterQuery],
                             object_filter_queries: List[FilterQuery]) -> List[S3BucketMetadata]:
        buckets = self.__s3_bucket_repository.find_all_buckets()
        filtered_buckets = self.__filter_buckets(bucket_filter_queries, buckets)

        object_filters = list(map(self.__s3_object_filter_factory.make_filter, object_filter_queries))

        return [self.__map_to_S3BucketMetadata(bucket.filter_objects(object_filters)) for bucket in filtered_buckets]

    def __filter_buckets(self, bucket_filter_queries: List[FilterQuery],
                         buckets: Iterable[S3Bucket]) -> Iterable[S3Bucket]:
        bucket_filters = map(self.__s3_bucket_filter_factory.make_filter, bucket_filter_queries)
        for bucket_filter in bucket_filters:
            buckets = filter(bucket_filter.filter, buckets)

        return buckets

    def __map_to_S3BucketMetadata(self, s3_bucket: S3Bucket) -> S3BucketMetadata:
        last_modified_date: Union[None, datetime] = None

        try:
            last_modified_date = s3_bucket.get_most_recent_object().last_modified_date
        except:
            pass

        return S3BucketMetadata(
            name=s3_bucket.name,
            creation_date=s3_bucket.creation_date,
            region=s3_bucket.region,
            nb_of_files=s3_bucket.get_nb_files(),
            total_files_size_in_bytes=s3_bucket.get_total_size_in_bytes(),
            last_modified_date_of_most_recent_file=last_modified_date
        )
