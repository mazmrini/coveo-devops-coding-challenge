# Coveo DevOps Challenge

[Link to challenge](https://www.github.com/coveo/devops-coding-challenge)


## Setup

### Dependencies
```bash
pip install -r requirements.txt
pip install -r test.requirements.txt
```

### Credentials
Refer to [boto3 documentation (env variables)](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#environment-variables)

## Example
```bash
# raw
python s3cli

# with options
python s3cli \
     sizeIn=b|mb|kb|gb \
     bucket_name=string \
     bucket_region=ca-central-1|us-east-2|us-west-2|... \
     file_regexp="\.png$" \
     file_storage=STANDARD|REDUCED_REDUNDANCY|STANDARD_IA|ONEZONE_IA|...
```

## Run tests
```bash
// run setup

cd s3cli
python -m unittest
```

## Run linting

```bash
// run setup

mypy s3cli
```
